package lesson1;

import java.util.Scanner;

public class TextDisplay {
	public static void main(String[] args) {
		String textDisplay;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Nhập text :  ");
		textDisplay = scanner.nextLine();

		System.out.println("Dòng văn bản người dùng vừa nhập là : " + textDisplay);
	}

}
